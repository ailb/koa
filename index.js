const Koa = require('koa'),
  app = new Koa(),
  router = require('./router/router'),
  view = require('koa-static');

app.use(async (ctx, next) => {
  let s = new Date().getTime();
  await next();
  console.log(` ${new Date().getTime() - s} ms ${ctx.method} ${ctx.path}`);
});
app.use(
  view('./views', {
    gzip: true,
  })
);

app.use(router.routes()).use(router.allowedMethods());
app.listen(8080);
