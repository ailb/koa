const router = new require("koa-router")();
const url = require("./urllib-sync").request;

function bing() {
  let u = JSON.parse(
    url("https://cn.bing.com/HPImageArchive.aspx?format=js&idx=0&n=1").data.toString()
  ).images[0].url;
  return `//cn.bing.com${u}`;
}

router.get("/images/bing", async (ctx, next) => {
  // 'https://cn.bing.com/HPImageArchive.aspx?format=js&idx=0&n=1';
  ctx.status = 302;
  ctx.redirect(bing());

  await next();
});
module.exports = router;
